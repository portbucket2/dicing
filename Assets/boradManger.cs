﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class boradManger : MonoBehaviour
{
    public GameObject guti;
    public GameObject[] blocks;
    public GameObject dice;
    public GameObject highlightBlock;
    public GameObject burstParticle;

    public int gutiValue;
    public int oldGutiValue;
    public int newGutiValue;

    public int upValue;
    public int downValue;
    public int rightValue;
    public int leftValue;

    public int currentValue;
    public bool gutiMoved;
    public bool diceUpdated;

    public bool isGameOver;

    diceMover dicemove;

    // Start is called before the first frame update
    void Start()
    {
        gutiValue =oldGutiValue =  0 ;
        //currentValue = dice.GetComponent<diceMover>().currentValue;

        dicemove = dice.GetComponent<diceMover> ();
    }

    // Update is called once per frame
    void Update()
    {
        if ( NewManager.instance.interactible )
        {
            getSensorValues ();
            SeeFuturePos ();
        }
        senseDiceUpdate();
        UpdateGutiValue();
        senseGameOver();
    }

    void UpdateGutiValue()
    {
        if (!Input.GetMouseButton(0) && diceUpdated == false)
        {
            //currentValue = dice.GetComponent<diceMover>().currentValue;
        }
        if (gutiMoved == false && diceUpdated == true)
        {
            //newGutiValue = oldGutiValue + currentValue;
            //if(newGutiValue >= 0 && newGutiValue < blocks.Length)
            //{
            //    oldGutiValue = newGutiValue;
            //}
            //else if(newGutiValue < 0)
            //{
            //    newGutiValue = blocks.Length + newGutiValue;
            //    oldGutiValue = newGutiValue;
            //}
            //else if (newGutiValue >= blocks.Length)
            //{
            //    newGutiValue = -blocks.Length + newGutiValue;
            //    oldGutiValue = newGutiValue;
            //}
            //gutiValue = newGutiValue;
            //gutiValue = Mathf.Clamp(gutiValue, 0, blocks.Length -1);
            oldGutiValue = newGutiValue;
            //guti.transform.localPosition = blocks[gutiValue].transform.localPosition;
            //gutiMoved = true;

            NewManager.instance.executeMove ();
            
            diceUpdated = false;
        }
    }

    void senseDiceUpdate()
    {
        if(dice.GetComponent<diceMover>().goAhead == true )
        {

            diceUpdated = true;
            //gutiValue += currentValue;
            dice.GetComponent<diceMover>().goAhead = false;
            dice.GetComponent<diceMover>().goBack = false;

        }
        else if (dice.GetComponent<diceMover>().goBack == true)
        {

            diceUpdated = true;
            //gutiValue -= currentValue;
            currentValue *= -1;
            dice.GetComponent<diceMover>().goAhead = false;
            dice.GetComponent<diceMover>().goBack = false;

        }
    }
    void SeeFuturePos()
    {
        if (Input.GetMouseButton(0))
        {
            int negFac;
            if (dice.GetComponent<diceMover>().downMode || dice.GetComponent<diceMover>().leftMode)
            {
                negFac = -1;
            }
            else
            {
                negFac = 1;
            }

            if(Mathf.Abs(dice.GetComponent<diceMover>().releaseRotValue) > 45)
            {
                newGutiValue = oldGutiValue + currentValue * negFac;
                NewManager.instance.setindicatorindex ( currentValue * negFac );
            }
            else
            {
                newGutiValue = oldGutiValue;
                NewManager.instance.setindicatorindex ( 0 );
            }
            //newGutiValue = oldGutiValue + currentValue* negFac;
            if (newGutiValue >= 0 && newGutiValue < blocks.Length)
            {
                newGutiValue = newGutiValue;
            }
            else if (newGutiValue < 0)
            {
                newGutiValue = blocks.Length + newGutiValue;
                //oldGutiValue = newGutiValue;
            }
            else if (newGutiValue >= blocks.Length)
            {
                newGutiValue = -blocks.Length + newGutiValue;
                //oldGutiValue = newGutiValue;
            }
            gutiValue = newGutiValue;
            //highlightBlock.transform.localPosition = blocks[gutiValue].transform.localPosition;
        }
        else
        {
            highlightBlock.transform.localPosition = guti.transform.localPosition;
        }
        
    }
    void senseGameOver()
    {
        if(guti.transform.localPosition == blocks[gutiValue].transform.localPosition && blocks[gutiValue].GetComponent<blockProperties>().locked == true && isGameOver == false)
        {
            burstParticle.transform.localPosition = blocks[gutiValue].transform.localPosition;
            burstParticle.GetComponent<ParticleSystem>().Play();
            guti.SetActive(false);
            highlightBlock.SetActive(false);
            isGameOver = true;

            Invoke("Restart", 1f);
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    void getSensorValues()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            upValue =  dice.GetComponent<diceMover>().upSensorValue    ;
            downValue = dice.GetComponent<diceMover>().downSensorValue  ;
            rightValue = dice.GetComponent<diceMover>().rightSensorValue ;
            leftValue = dice.GetComponent<diceMover>().leftSensorValue  ;

            
        }

        if (dice.GetComponent<diceMover>().downMode)
        {
            currentValue = upValue;
        }
        else if (dice.GetComponent<diceMover>().upMode)
        {
            currentValue = downValue;
        }
        else if (dice.GetComponent<diceMover>().rightMode)
        {
            currentValue = leftValue;
        }
        else if (dice.GetComponent<diceMover>().leftMode)
        {
            currentValue = rightValue;
        }
    }

    void executeMove ()
    {

    }
}
