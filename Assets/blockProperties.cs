﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blockProperties : MonoBehaviour
{
    public GameObject trap;
    public GameObject platform;
    public Animator trapAnimator;
    public bool locked;
    // Start is called before the first frame update
    void Start()
    {
        //trapAnimator = GetComponent<Animator> ();
        platform.SetActive ( !locked );
        trap.SetActive ( locked );
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pressed ()
    {
        if ( locked )
        {
            trapAnimator.SetTrigger ( "traptrigger" );
        }
    }
}
