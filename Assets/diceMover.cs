﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class diceMover : MonoBehaviour
{
    public GameObject diceHolderSide;
    public GameObject diceHolderUpdown;
    public GameObject dicePieceDummy;
    public GameObject dicePieceOriginal;
    public Vector2 mousePos;
    public Vector2 oldMousePos;
    public Vector2 newMousePos;
    public Vector2 swipeTravel;

    public Camera swipeCam;

    public float minSwipeDistance;
    public float swipeDistance;

    public bool swipeUp;
    public bool swipeDown;
    public bool swipeRight;
    public bool swipeLeft;

    public bool swiping;

    public bool upDownMode;
    public bool leftRightMode;

    public bool upMode;
    public bool downMode;
    public bool rightMode;
    public bool leftMode;

    public Vector3 diceHolderUpdownRot;
    public Vector3 diceHolderUpdownRotOld;
    public Vector3 diceHolderSideRot;
    public Vector3 diceHolderSideRotOld;

    public float upDownRot;
    public float leftRightRot;

    public GameObject highlightCube;

    [SerializeField] MeshRenderer toparrow;
    [SerializeField] MeshRenderer bottomarrow;
    [SerializeField] MeshRenderer leftarrow;
    [SerializeField] MeshRenderer rightarrow;

    [SerializeField] Material white;
    [SerializeField] Material greenhighlight;

    public bool goAhead;
    public bool goBack;
    public float releaseRotValue;

    public int upSensorValue;
    public int downSensorValue;
    public int rightSensorValue;
    public int leftSensorValue;
    //public int currentValue;

    public int currentValue;
    public GameObject[] sensors;

    Quaternion revertedrotation;
    // Start is called before the first frame update
    void Start()
    {
        highlightCube.transform.localPosition = new Vector3(0, 0, 0);
        currentValue = sensors[4].GetComponent<sensorManager>().sideValue;
        toparrow.sharedMaterial = white;
        bottomarrow.sharedMaterial = white;
        leftarrow.sharedMaterial = white;
        rightarrow.sharedMaterial = white;
    }

    // Update is called once per frame
    void Update()
    {
        
        if ( NewManager.instance.interactible )
        {
            getSensorValues ();
            GetMovementCommands ();
            GetSwipe ();
        }
        RotateDice();
        dicePieceOriginal.transform.rotation = Quaternion.Lerp(dicePieceOriginal.transform.rotation, dicePieceDummy.transform.rotation, Time.deltaTime * 10f);
    }
    void GetSwipe()
    {
        
        mousePos = Input.mousePosition / Screen.width;
        if (Input.GetMouseButtonDown(0) )
        {
            oldMousePos = newMousePos = mousePos;

            revertedrotation = dicePieceDummy.transform.rotation;
        }
        if (Input.GetMouseButton(0) )
        {
            newMousePos = mousePos;
            swipeTravel = newMousePos - oldMousePos;
            if (Mathf.Abs(swipeTravel.x) > Mathf.Abs(swipeTravel.y) && Mathf.Abs(swipeTravel.x) > minSwipeDistance && upDownMode == false)
            {
                if (swipeTravel.x > 0)
                {
                    rightMode = true;
                    leftMode = false;
                    highlightCube.transform.localPosition = new Vector3(1.4f, 0, 0);
                    toparrow.sharedMaterial = white;
                    bottomarrow.sharedMaterial = white;
                    leftarrow.sharedMaterial = white;
                    rightarrow.sharedMaterial = greenhighlight;
                }
                else
                {
                    leftMode = true;
                    rightMode = false;
                    highlightCube.transform.localPosition = new Vector3(-1.4f, 0, 0);
                    toparrow.sharedMaterial = white;
                    bottomarrow.sharedMaterial = white;
                    leftarrow.sharedMaterial = greenhighlight;
                    rightarrow.sharedMaterial = white;


                }
                upDownMode = false;
                leftRightMode = true;

            }
            else if (Mathf.Abs(swipeTravel.y) > Mathf.Abs(swipeTravel.x) && Mathf.Abs(swipeTravel.y) > minSwipeDistance && leftRightMode == false)
            {
                if (swipeTravel.y > 0)
                {
                    upMode = true;
                    downMode = false;
                    highlightCube.transform.localPosition = new Vector3(0, 0, 1.4f);
                    toparrow.sharedMaterial = greenhighlight;
                    bottomarrow.sharedMaterial = white;
                    leftarrow.sharedMaterial = white;
                    rightarrow.sharedMaterial = white;


                }
                else
                {
                    upMode = false;
                    downMode = true;
                    highlightCube.transform.localPosition = new Vector3(0, 0, -1.4f);
                    toparrow.sharedMaterial = white;
                    bottomarrow.sharedMaterial = greenhighlight;
                    leftarrow.sharedMaterial = white;
                    rightarrow.sharedMaterial = white;

                }
                leftRightMode = false;
                upDownMode = true;

            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            //ResetSwipes();
            ResetModes();
            ReleaseDiceDrag();
            swipeTravel = new Vector2(0, 0);
            swiping = false;
            highlightCube.transform.localPosition = new Vector3(0, 0, 0);
            toparrow.sharedMaterial = white;
            bottomarrow.sharedMaterial = white;
            leftarrow.sharedMaterial = white;
            rightarrow.sharedMaterial = white;
        }
    }

    void ResetSwipes()
    {
        swipeUp = false;
        swipeDown = false;
        swipeRight = false;
        swipeLeft = false;
    }
    void ResetModes()
    {
        upDownMode = false;
        leftRightMode = false;

        upMode = false;
        downMode = false;
        rightMode = false;
        leftMode = false;


    }

    void RotateDice()
    {
        //upDownRot = Mathf.Clamp(upDownRot, -90f, 90f);
        diceHolderUpdown.transform.localRotation = Quaternion.Euler(diceHolderUpdownRot);
        diceHolderSide.transform.localRotation = Quaternion.Euler(diceHolderSideRot);
        if (upDownMode)
        {
            dicePieceDummy.transform.SetParent(diceHolderUpdown.transform);
            //diceHolderUpdown.transform.rotation = Quaternion.Euler(diceHolderUpdownRot);
            upDownRot = swipeTravel.y * Mathf.Rad2Deg *6f;
            upDownRot = Mathf.Clamp(upDownRot, -90f, 90f);
            diceHolderUpdownRot.x = diceHolderUpdownRotOld.x + upDownRot ;
            
        }
        else if (leftRightMode)
        {
            dicePieceDummy.transform.SetParent(diceHolderSide.transform);
            leftRightRot = -swipeTravel.x * Mathf.Rad2Deg * 6f;
            leftRightRot = Mathf.Clamp(leftRightRot, -90f, 90f);
            diceHolderSideRot.z = diceHolderSideRotOld.z + leftRightRot;
            
            diceHolderUpdownRotOld.z = diceHolderSideRot.z;
        }
    }

    void ReleaseDiceDrag()
    {

        if (upDownRot > 45)
        {
            upDownRot = 90;

        }
        else if (upDownRot < -45)
        {
            upDownRot = -90;
        }
        else
        {
            upDownRot = 0;
        }
        diceHolderUpdownRotOld.x = diceHolderUpdownRotOld.x + upDownRot ;
        upDownRot = 0;
        diceHolderUpdownRot.x = diceHolderUpdownRotOld.x + upDownRot;
        

        if (leftRightRot > 45)
        {
            leftRightRot = 90;

        }
        else if (leftRightRot < -45)
        {
            leftRightRot = -90;
        }
        else
        {
            leftRightRot = 0;
        }
        diceHolderSideRotOld.z = diceHolderSideRotOld.z + leftRightRot;
        leftRightRot = 0;
        diceHolderSideRot.z = diceHolderSideRotOld.z + leftRightRot;
        //dicePieceRot.z = diceHolderRot.z;
        //dicePiece.transform.SetParent(this.transform);
        //dicePieceRot.z = diceHolderRot.z;


    }

    void GetMovementCommands()
    {
        getCenterSensorValue();
        releaseRotValue = upDownRot + leftRightRot;
        if (Input.GetMouseButtonUp(0))
        {
            //releaseRotValue = upDownRot + leftRightRot;
            if((upDownMode && releaseRotValue > 45) )
            {
                goAhead = true;
                //nextValue = downSensorValue;
            }
            else if ((upDownMode && releaseRotValue < -45))
            {
                goBack = true;
                //nextValue = upSensorValue;
            }
            else if ((leftRightMode && releaseRotValue < - 45))
            {
                goAhead = true;
                //nextValue = leftSensorValue;
            }
            else if ((leftRightMode && releaseRotValue > 45))
            {
                goBack = true;
                //nextValue = rightSensorValue;
            }
        }

    }

    void getSensorValues()
    {
        if (Input.GetMouseButtonDown(0))
        {
            upSensorValue = sensors[0].GetComponent<sensorManager>().sideValue;
            downSensorValue = sensors[1].GetComponent<sensorManager>().sideValue;
            rightSensorValue = sensors[2].GetComponent<sensorManager>().sideValue;
            leftSensorValue = sensors[3].GetComponent<sensorManager>().sideValue;
        }
    }

    void getCenterSensorValue()
    {
        if (!Input.GetMouseButton(0))
        {
            currentValue = sensors[4].GetComponent<sensorManager>().sideValue;
        }
    }

    public void revertdicerotation ()
    {
        dicePieceDummy.transform.rotation = revertedrotation;
    }
}
