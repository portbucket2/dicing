﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sensorManager : MonoBehaviour
{
    public Transform sensorHolder;

    public RaycastHit hit;
    public float raycastRange;
    public LayerMask diceSideLayer;

    public int sideValue;
    public GameObject[] sideNumberMeshes;
    
    // Start is called before the first frame update
    void Start()
    {
        Raycasting();
    }

    // Update is called once per frame
    void Update()
    {
        //Raycasting();
        if (!Input.GetMouseButton(0))
        {
            Raycasting();
        }
        
    }

    void Raycasting()
    {
        if (Physics.Raycast(sensorHolder.transform.position, transform.forward, out hit, raycastRange, diceSideLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            
            //Debug.Log("Did Hit");
            
            
            sideValue = int.Parse(hit.transform.gameObject.tag);
            GetSideValue();

        }
    }

    void GetSideValue()
    {
        HideAllNumbers();
        sideNumberMeshes[sideValue -1].SetActive(true);
    }

    void HideAllNumbers()
    {
        for (int i = 0; i < sideNumberMeshes.Length; i++)
        {
            sideNumberMeshes[i].SetActive(false);
        }
    }
}
