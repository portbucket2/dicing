﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewManager : MonoBehaviour
{

    public static NewManager instance;

    [SerializeField]float gapdistance;
    [SerializeField] float gaptraversaltime;
    [SerializeField] float reversaltime;
    public bool[] blocklist; //false = safe

    [SerializeField] float risingdistance;

    [SerializeField] Transform verticalmover;
    [SerializeField] Transform guti;
    [SerializeField] Transform gutichild;
    [SerializeField] Transform goalIndicator;

    public bool interactible = true;

    [SerializeField] int currentPositionIndex = 7;
    [SerializeField] int targetPositionIndex = 20;

    public int indicatorindex;
    public int indicatororigin;

    [SerializeField] Transform[] indicators = new Transform[6];
    blockProperties[] blockcontrollers;

    boradManger bdman;

    // Start is called before the first frame update

    private void Awake ()
    {
        guti.localPosition = currentPositionIndex * gapdistance * Vector3.right;
        goalIndicator.localPosition = targetPositionIndex * gapdistance * Vector3.right;
        bdman = GetComponent<boradManger> ();
        blockcontrollers = new blockProperties[bdman.blocks.Length];
        for ( int i = 0; i < bdman.blocks.Length; i++ )
        {
            bdman.blocks[i].transform.localPosition = gapdistance * ( i + 1 ) * Vector3.right;
            blockcontrollers[i] = bdman.blocks[i].GetComponent<blockProperties> ();
            blockcontrollers[i].locked = blocklist[i];
        }

        indicatororigin = currentPositionIndex;

        instance = this;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ( interactible )
        {
            if ( Input.GetKeyDown ( KeyCode.Q ) )
            {
                StartCoroutine ( startbounce(6,true) );
                setindicatorindex ( 6 );
            }
            else if ( Input.GetKeyDown ( KeyCode.W ) )
            {
                StartCoroutine ( startbounce ( 5, true ) );
                setindicatorindex ( 5 );
            }
            else if ( Input.GetKeyDown ( KeyCode.E ) )
            {
                StartCoroutine ( startbounce ( 4, true ) );
                setindicatorindex ( 4 );
            }
            else if ( Input.GetKeyDown ( KeyCode.R) )
            {
                StartCoroutine ( startbounce ( 3, true ) );
                setindicatorindex ( 3 );
            }
            else if ( Input.GetKeyDown ( KeyCode.T) )
            {
                StartCoroutine ( startbounce ( 2, true ) );
                setindicatorindex ( 2 );
            }
            else if ( Input.GetKeyDown ( KeyCode.Y) )
            {
                StartCoroutine ( startbounce ( 1, true ) );
                setindicatorindex ( 1 );
            }
            else if ( Input.GetKeyDown ( KeyCode.A) )
            {
                StartCoroutine ( startbounce ( 6, false ) );
                setindicatorindex ( -6 );
            }
            else if ( Input.GetKeyDown ( KeyCode.S) )
            {
                StartCoroutine ( startbounce ( 5, false ) );
                setindicatorindex ( -5 );
            }
            else if ( Input.GetKeyDown ( KeyCode.D) )
            {
                StartCoroutine ( startbounce ( 4, false ) );
                setindicatorindex ( -4 );
            }
            else if ( Input.GetKeyDown ( KeyCode.F) )
            {
                StartCoroutine ( startbounce ( 3, false ) );
                setindicatorindex ( -3 );
            }
            else if ( Input.GetKeyDown ( KeyCode.G) )
            {
                StartCoroutine ( startbounce ( 2, false ) );
                setindicatorindex ( -2 );
            }
            else if ( Input.GetKeyDown ( KeyCode.H) )
            {
                StartCoroutine ( startbounce ( 1, false ) );
                setindicatorindex ( -1 );
            }
        }
        setindicator ();
    }

    public void setindicatorindex (int index)
    {
        indicatorindex = index;
        indicatororigin = currentPositionIndex;
    }

    void setindicator ()
    {
        if ( indicatorindex != 0 && indicatorindex < 7 && indicatorindex > -7 )
        {
            if ( indicatorindex > 0 )
            {
                for ( int i = 1; i <= indicatorindex; i++ )
                {
                    indicators[i - 1].gameObject.SetActive ( true );
                    indicators[i - 1].localPosition = ( indicatororigin + i ) * gapdistance * Vector3.right;
                }
                for ( int i = indicatorindex + 1; i < 7; i++ )
                {
                    indicators[i - 1].gameObject.SetActive ( false );
                }
            }
            else
            {
                for ( int i = -1; i >= indicatorindex; i-- )
                {
                    indicators[-i - 1].gameObject.SetActive ( true );
                    indicators[-i - 1].localPosition = ( indicatororigin + i ) * gapdistance * Vector3.right;
                }
                for ( int i = -indicatorindex + 1; i < 7; i++ )
                {
                    indicators[i - 1].gameObject.SetActive ( false );
                }
            }
        }
        else
        {
            for ( int i = 0; i < indicators.Length; i++ )
            {
                indicators[i].gameObject.SetActive ( false );
            }
        }
    }

    IEnumerator bouncer ( int numbounce, float bouncetime, float bounceheight, Transform bouncetarget, bool forwardir )
    {
        if ( numbounce > 0 && numbounce != 1 )
        {
            yield return StartCoroutine ( bouncer ( numbounce - 1, bouncetime, bounceheight, bouncetarget, forwardir ) );
        }

        float t = 0;

        orientguti ( forwardir );
        int one = forwardir ? 1 : -1;

        while ( t <= bouncetime )
        {


            t += Time.deltaTime;

            float grav = 2* bounceheight / (bouncetime * bouncetime);

            float d = 0.5f * grav * t * ( bouncetime - t );

            bouncetarget.localPosition = d * Vector3.up;

            guti.localPosition = (currentPositionIndex * gapdistance + one * gapdistance * (t/bouncetime)) * Vector3.right;

            yield return null;


        }
        bouncetarget.localPosition = Vector3.zero;
        currentPositionIndex += one;
        guti.localPosition = currentPositionIndex * gapdistance * Vector3.right;
        blockcontrollers[currentPositionIndex-1].pressed ();
    }

    IEnumerator startbounce ( int bouncenum, bool dirforward )
    {
        interactible = false;

        int prevpos = currentPositionIndex;
        if ( bouncenum > 6 )
        {
            bouncenum = 6;
        }
        yield return StartCoroutine ( bouncer ( bouncenum, gaptraversaltime, risingdistance, verticalmover, dirforward ) );
        if(currentPositionIndex == targetPositionIndex)
        {
            // win
        }
        else if ( blocklist[currentPositionIndex - 1] )
        {
            bdman.dice.GetComponent<diceMover> ().revertdicerotation ();

            float grav = 2 * risingdistance / ( gaptraversaltime * gaptraversaltime );

            float v = grav * reversaltime;

            float t = 0;

            while ( t <= 2 * reversaltime )
            {
                t += Time.deltaTime;

                float distvert = v * t - 0.5f * grav * t * t;
                float disthor = Mathf.Lerp ( currentPositionIndex, prevpos, t / ( 2 * reversaltime ) ) * gapdistance;

                verticalmover.localPosition = distvert * Vector3.up;
                guti.localPosition = disthor * Vector3.right;
                verticalmover.localEulerAngles = 360 * ( t / ( 2 * reversaltime ) ) * Vector3.forward;
                //verticalmover.localEulerAngles = (t<reversaltime?1:-1)* 180 * ( distvert / ( v * reversaltime - 0.5f * grav * reversaltime * reversaltime ) ) * Vector3.forward;

                yield return null;
            }

            verticalmover.localPosition = Vector3.zero;
            verticalmover.localEulerAngles = Vector3.zero;
            currentPositionIndex = prevpos;
            guti.localPosition = currentPositionIndex * gapdistance * Vector3.right;
            setindicatorindex ( 0 );


        }
        else
        {

            yield return new WaitForSeconds ( 0.2f );
            setindicatorindex ( 0 );
        }

        interactible = true;
    }

    void orientguti (bool forward)
    {
        int one = forward ? 1 : -1;

        gutichild.localEulerAngles = one * 90 * Vector3.up;
    }

    public void executeMove ()
    {
        if ( indicatorindex > 0 )
        {
            StartCoroutine ( startbounce ( indicatorindex, true ) );
        }
        else if ( indicatorindex < 0 )
        {
            StartCoroutine ( startbounce ( -indicatorindex, false ) );
        }
    }

    void updatepredictedmove ()
    {

    }
}
